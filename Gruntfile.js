/*global module:true */
module.exports = function (grunt) {
	'use strict';

	var DEV_ROOT = './src',
		DEV_SCRIPTS = DEV_ROOT + '/scripts',

		DIST_ROOT = './dist',

	// Array of Script files to include into build
		APP_SCRIPTS = [
			// module dependancies
			DEV_SCRIPTS + '/modules/*.js',

			// app
			DEV_SCRIPTS + '/app.js',

			// services
			DEV_SCRIPTS + '/services/*.js',

			// controllers
			DEV_SCRIPTS + '/controllers/*.js',

			// directives
			DEV_SCRIPTS + '/directives/*.js',

			// router - goes last
			DEV_SCRIPTS + '/router/*.js',
			DEV_SCRIPTS + '/libs/angular-route-1.2.22.min.js',
			DEV_SCRIPTS + '/libs/socket.io.min.js'
		];

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	require('matchdep').filterDev('assemble').forEach(grunt.loadNpmTasks);

	// GRUNT PLUGIN CONFIGURATION
	grunt.initConfig({

		// Get package data
		pkg: grunt.file.readJSON('package.json'),

		// Banners to prepend to built files
		banner: {
			ecma: '/**\n' +
					' *  <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
					'<%= pkg.homepage ? " *  " + pkg.homepage + "\\n" : "" %>' +
					' *  Copyright (c)<%= grunt.template.today("yyyy") %> <%= pkg.author.name %>\n' +
					' */ \n\n'
		},

		// JSHINT 
		jshint: { /* Lint the Gruntfile, all modules and specs except JQuery. */
			all: [
				'Gruntfile.js',
				DEV_SCRIPTS + '/**/*.js',

				// Not libs
				'!' + DEV_SCRIPTS + '/libs/**/*.js'
			],
			options: {
				bitwise: false,
				browser: true,
				curly: true,
				eqeqeq: true,
				es3: true, /* Assume site must work in IE6/7/8. */
				forin: true,
				globals: { /* Require and Jasmine's Globals. Note that $ is not permitted. */
					'requirejs': false, /* Require */
					'require': false,
					'define': false,
					'describe': false, /* Jasmine */
					'xdescribe': false,
					'it': false,
					'xit': false,
					'beforeEach': false,
					'afterEach': false,
					'jasmine': false,
					'spyOn': false,
					'expect': false,
					'waitsFor': false
				},
				immed: true,
				indent: 4,
				jquery: true,
				latedef: true,
				maxdepth: 2,
				newcap: true,
				noarg: true,
				noempty: false,
				onevar: true,
				plusplus: false,
				quotmark: 'single', /* Use backslashes if they\'re needed. */
				regexp: false,
				regexdash: true,
				'-W099': true, // suppresses mixed spaces and tabs errors - have both.
				strict: false, /* Use one 'use strict'; per file. */
				trailing: true, /* Turn 'show whitespace' on in your editor. */
				undef: false,
				unused: false
			}
		},

		// CLEAN Output dir
		clean: {
			options: {
				force: true
			},
			src: [DIST_ROOT + '/']
		},

		// COPY 
		copy: {
			resources: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['favicon.ico']
				}]
			},

			fonts: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['fonts/**/*']
				}]
			},

			scripts: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['scripts/libs/**/*']
				}]
			},

			images: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['images/**']
				}]
			},

			videos: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['videos/**']
				}]
			},

			views: {
				files: [{
					expand: true,
					dest: DIST_ROOT,
					cwd: DEV_ROOT,
					src: ['views/**']
				}]
			}
		},

		// SASS COMPILATION
		sass: {
			options: {
				banner: '<%= banner.ecma %>'
			},

			dev: {
				files: {
					'dist/styles/main.css': 'src/styles/main.scss'
				},
				options: {
					style: 'expanded',
					lineNumbers: true
				}
			},
			prod: {
				files: {
					'dist/styles/main.css': 'src/styles/main.scss'
				},
				options: {
					style: 'compressed'
				}
			}
		},

		// FILE CONCATINATION 
		concat: {
			options: {
				banner: '<%= banner.ecma %>'
			},
			scripts: {
				src: APP_SCRIPTS,
				dest: DIST_ROOT + '/scripts/app.js'
			}
		},

		// SCRIPT MINIFICATION
		uglify: {
			options: {
				banner: '<%= banner.ecma %>'
			},
			scripts: {
				files: {
					'dist/scripts/app.js': APP_SCRIPTS
				}
			}
		},

		// ASSEMBLE THE MARKUP
		assemble: {
			options: {
				assets: DIST_ROOT,
				data: ['package.json'],
				partials: DEV_ROOT + '/templates/modules/*.hbs',
				ext: '.html',
				helpers: DEV_ROOT + '/templates/helpers/*.js',
				layout: DEV_ROOT + '/templates/layout/master.hbs',
				removeHbsWhitespace: true
			},
			pages: {
				options: {
					production: false
				},
				files: [{
					expand: true,
					cwd: DEV_ROOT + '/templates/pages',
					src: ['*.hbs'],
					dest: DIST_ROOT
				}]
			}
		},

		// WATCH for file changes
		watch: {
			options: {
				livereload: true
			},
			data: {
				files: ['data/**/*.json'],
				tasks: ['build-dev'],
				options: {
					spawn: false
				}
			},
			configuration: {
				files: ['Gruntfile.js', DEV_ROOT + '**/*.json'],
				tasks: ['build-dev'],
				options: {
					spawn: false
				}
			},
			images: {
				files: [DEV_ROOT + '/images/**'],
				tasks: ['copy:images'],
				options: {
					spawn: false
				}
			},
			videos: {
				files: [DEV_ROOT + '/videos/**'],
				tasks: ['copy:videos'],
				options: {
					spawn: false
				}
			},
			scripts: {
				files: [DEV_ROOT + '/scripts/**/*.js'],
				tasks: ['concat:scripts', 'jshint'],
				options: {
					spawn: false
				}
			},
			styles: {
				files: [DEV_ROOT + '/styles/**/*.{scss,sass}'],
				tasks: ['sass:dev'],
				options: {
					spawn: true
				}
			},
			markup: {
				files: [DEV_ROOT + '/templates/**/*.hbs', DEV_ROOT + '/templates/**/*.js'],
				tasks: ['assemble'],
				options: {
					spawn: false
				}
			},
			views: {
				files: [DEV_ROOT + '/views/**/*.html'],
				tasks: ['copy:views'],
				options: {
					spawn: false
				}
			}
		}
	});

	// ENVIRONMENT TASKS

	// development build - run 'grunt dev'
	grunt.registerTask('dev', ['clean', 'build-dev']);
	grunt.registerTask('build-dev', ['sass:dev', 'assemble', 'copy', 'concat:scripts', 'jshint']);

	// production build - run 'grunt prod'
	grunt.registerTask('prod', ['clean', 'build-prod']);
	grunt.registerTask('build-prod', ['sass:prod', 'copy', 'uglify', 'assemble']);


	// default task - Production to prevent development code going live
	grunt.registerTask('default', 'prod');
};
