﻿# Realtime app demo

This project accompanies a blog post I wrote, that can be found here;
[https://will-hancock.ghost.io](https://will-hancock.ghost.io)

Building a simple realtime data app with AngualrJS, node.js and websockets.

Uses Grunt to build the static HTML, JS and CSS files.

- SASS for CSS
- Assemble for HTML
- JavaScript + jQuery

### Technical contacts
* Will Hancock - Web Developer - [http://will-hancock.ghost.io/](http://will-hancock.ghost.io/)

## Setup and Development
### System Requirements
Install the following;

* Environment: [Node.js](http://nodejs.org/)
* Task Runner: [Grunt.js](http://gruntjs.com/getting-started)
* CSS precompiler: [Ruby](https://www.ruby-lang.org/en/) and [SASS](http://sass-lang.com/)

### Build instructions
In your terminal;

* Checkout the repo ```git clone git clone git clone https://bitbucket.org/will-hancock/realtime-webapp-demo.git``` to your desired directory.
* Navigate to the build folder ```cd realtime-webapp-demo```
* Install the node modules ```npm install```
* Run a build; ```grunt``` or ```grunt prod``` - to build production ready code into the ```/dist/``` directory
* For Development run ```grunt dev``` or ```grunt watch```- uncompressed, unminified, debug options and a watch setup to build on file changes
* View ```/dist/index.html``` in your browser

And you are ready to go, the hard work setting up has been done, and you can now concentrate on making your site a winner.