﻿'use strict';
var express = require('express.io'),

  // Create app instance.
  app = express(),

  // Use the `PORT` environment variable, or port 3000
  PORT = process.env.PORT || 3000,

  WEB_ROOT = 'dist';


// Set Static Path
app.use(express.static(WEB_ROOT));

/* ROUTES
 **********************************************************************/
// Route / send back index.html
app.get('/', function (req, res) {
	res.sendFile('index.html');
});

// Redirect all paths to /
app.get('/*', function (req, res) {
	res.redirect('/');
});

/* START SERVER
**********************************************************************/
// Listen for sockets
app.http().io();

// Start the Chat Handling
app.chat = require('./server-chat')(app);

/* LISTEN FOR INCOMING REQUESTS
**********************************************************************/
app.listen(PORT, function () {
	console.log('Node server listening on port ' + PORT);
});

app.io.route('error', function (err) {
	console.log('There was an error in the application, catch to prevent bubbling up', err);
});