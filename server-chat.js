﻿/* CHAT ROOM FUNCTIONALITY */

var messages = [];

module.exports = function (app) {

	var addMessage = function (message) {

		// Add timestamp to message
		message.timestamp = new Date().getTime();

		// Add to message array
		messages.push(message);

		// Send message back to clients
		app.io.broadcast('chat:message:new', message);
		console.log('chat:message:new', message);
	}
	
	/* SOCKET ROUTES
	**********************************************************************/
	// listen for a new runner added
	app.io.route('chat:message:new', function (req) {
		addMessage(req.data);
	});
};
