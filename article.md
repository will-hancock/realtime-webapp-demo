﻿## Introduction
Over the last year working for a digital agency, I have had the privalage of building some exciting web apps for a leading sportswear company (not Adidas). In a change from building 'websites', these apps were built to provide interactivity at live sporting events, displaying realtime data on big screens integrated with some pretty challenging HTML5/CSS3 celebratory animations.  I learnt a lot about the current 'on trend' JavaScript technologies, namely AngularJS, Grunt, node.js and websockets.

In this post I want to show to Web developers of all levels, actually how easy it is to get started with all or any of the above technologies.  Even if you only discover one thing that you take into your daily working, this post will have been a success. I will try and explain a little about each of the technologies in the approach for any beginners.

During this post I will demonstrate each technology by building a simple chat client, demonstrating the flow of the data, and how Angular displays it.

##### What will we be using?
* [Grunt](http://gruntjs.com/) - a task runner to build the static site, concatinating JS, CSS, and building the HTML.
* [SASS](http://sass-lang.com/) - a CSS preprocessor. If you are not using SASS or LESS for your CSS, you are just making life harder for yourself!
* [Assemble](http://assemble.io/) - To modularise the HTML templates - You could just use a flat HTML file for this.
* [AngularJS](https://angularjs.org/) - Client-side JavaScript framework, great for Single Page Apps.
* [Node.js](http://nodejs.org/) - with Express - As a webserver - I'm a web developer, I don't like fudging with Apache, Tomcat, IIS whatever you use.  Node is just quick an easy. We wont be using a Database of any kind, thats for another day.
* [Socket.io](http://socket.io/) - This makes utilising websockets super easy.

I appreciate, a lot of web developers would find that list daunting, but do not worry, I will guide you through.

## Let's get Started!
Now, I have created myself base-project, just so that I don't have to start from scratch everytime.  A lot of developers do this, but they continue to keep on developing it, adding more and more and more until the first thing they have to do at the start of each project is remove all the stuff they don't need.  And if they don't remove all the stuff they don't need, well, slap on the wrist for them! Those base projects also make it hard for new developers to come in and understand what is going on and get started quickly. 

You can find and download my Angular base project here; [Base Project](https://bitbucket.org/will-hancock/base-project-angular) - feel free to use this as a base in future.

* Checkout the demo repo; [Demonstration project](https://bitbucket.org/will-hancock/realtime-webapp-demo) - if you dont have or use [Git](http://git-scm.com/), I strongly suggest you do! Even if you are a solitary bedroom developer, what if you computer explodes? Version your code and host it somewhere!! I use [Bitbucket](https://bitbucket.org) for its free private repos.

* Follow the [build instructions](https://bitbucket.org/will-hancock/realtime-webapp-demo/src/eb78bec4529c2249e33d6d5be4ce46271838742b/README.md?at=master) and see if you can get the project building.  You may have to install Node.js, Grunt, Ruby (Windows) and SASS - by following the links in the instructions.

The structure is quite simple; 

```
realtime-webapp-demo/
	data/
	dist/
    src/
	...
    README.md
```

* data to build the site sits in ```data/```
* working/development files sit in ```src/```
* output files to be deployed sit in ```dist/```

#### Dependancies
To build and run this app we are dependant on a few other libraries.  The dependancies for Grunt and Node are managed by the ```package.json```.  These dependancies get installed when you ran the ```npm install``` command earlier.

### Grunt
As mentioned earlier, Grunt is a JavaScript task runner.  We configure Grunt in the ```Gruntfile.js``` 

On running Grunt commands, grunt will;

* Clean out the ```dist``` output directory
* Compile the SASS .scss stylesheets and output the resulting css file
* Build and output our HTML files
* Copy any static resources to the output folder
* Concatinate all our JavaScript files and optionally minify them to the output folder
* And run JSHint against the JavaScript to ensure we are writing JavaScript to a good standard.
* Watch for any file changes to rebuild the project automatically.

For this we have two Grunt tasks; 
Use ```grunt dev``` for unminified files - easy to debug issues and ```grunt prod``` for production ready CSS and Javascript, nicely minified for better performance.


### The Node.js server
The server code sits in ```server.js```.

We will be using [express](http://expressjs.com/), as this contains everything needed to run a webserver to save starting from scratch, in particular [express.io](http://express-io.org/) as this contains socket.io for the corresponding server side websockets that we will talk about later. We include this using nodes require method;

```
var express = require('express.io')
```

We initialise the express app and await a request for '/' in which we want to return the index.html

```
app.get('/', function (req, res) {
	res.sendFile('index.html');
});
```

Building a node server to serve static content is pretty much as easy as that!
For more, just check out [nodeschool](http://nodeschool.io/).

We will put all of our chat specific code into ```chat.js```, this is brought into the server code again using node's require()

```
// Start the Chat Handling
app.chat = require('./chat')(app);
```

We didn't need to use require, we could have done it all inline, but its nice to seperate out node into smaller files.

#### Starting node
In another terminal window from where you ran grunt, navigate to your app directory ```realtime-webapp-demo/``` and run the command;

```
node server
```

or ```node server.js``` to give it its full command.  

You should see ```Node server listening on port 3000```
So simply go to http://localhost:3000/ in your web browser.  To be greated with your webapp.  If you do not see this, check to make sure nothing else is running on that port.

## Building [a Angular](https://www.youtube.com/watch?v=mFlPxIxmego) app
So we have our project setup and running, I hope it didn't cause you too much hassle.  A seasoned webdev used to working with node and grunt will be up and running inside of 2 minutes at this point. So those who this is new to, hopefully you can see the potential!

Angular, What is it? The Angular site says;
> AngularJS is what HTML would have been, had it been designed for building web-apps. Declarative templates with data-binding, MVW, MVVM, MVC, dependency injection and great testability story all implemented with pure client-side JavaScript!

But what does that mean? Basically, you write whatever HTML tags you want and easily hook it up to JavaScript functions and data, without you having to do the hard work.  Data changes, your webpage changes like magic. Watch this 3min video: [egghead.io](https://egghead.io/lessons/angularjs-binding)

##### Some terminology
Services - An Angular service or factory, should handle the data, contain the models if we are talking like a traditional MVC app.  Also it should contain the methods for retrieving data (AJAX or Websockets), storing the data, and exposing the data to any part of the app that requires it.

Controllers - Angular controllers should contain the business logic for any particular part of the app.  This may be the app as a whole, an entire view or even down to smaller areas.  Controllers are responsible for retrieving data from services and exposing it to your views.  No DOM access or manipulation should happen in a Controller.

Directives - An Angular directive is a way to append specific functionality onto an area or element.  They can be all different sizes from appending some functionality onto a ```<video>``` tag, or creating an entire widget e.g. ```<carousel></carousel>``` 

$scope - The scope is a way of exposing data and methods to the current view state(s).  Either on an app level by using the shortcut $rootScope, or at a controller level just by accessing $scope.  $scope is accessable directly from an angular template.

templates - Angular templates contain the markup to display an angular view.

#### Project structure

If we take a look into the file structure for ```src/scripts``` we can see that we group our files by their type.
```
scripts/
	controllers/
	directives/
	libs/
	modules/
	router/
	services/
```
This is how Angular developers used to work, and is very common when coming from other MVC backgrounds, effectively sperating Models, from Views, from Controllers.
However, if you are working on a single component, say a carousel, you will be working on files, one in controllers, one in directives, and another in your views or templates, and you quickly find you are jumping all over the place.  And reuse between projects means you have to go and cherry pick all the files you want to copy.
Now, developers are really taking hold, and code reuse and modularisation is an important topic. You should look at grouping your files by componenet
e.g.
```
scripts/
	Feed/ 
		_feed.html 
		FeedController.js 
		FeedEntryDirective.js 
		FeedService.js 
	Login/ 
		_login.html 
		LoginController.js 
		LoginService.js 
	Shared/ 
		CapatalizeFilter.js
``` 
This article my Mark Meyer covers this nicely http://www.airpair.com/angularjs/posts/top-10-mistakes-angularjs-developers-make#jTbtGtbqMOiEhL30.99

But this app, isn't very big, and I think it will help understand the seperation of concerns better, so we will continue with the former, strategy.

### Let's initialise our Angular app;
Include Angular into the page

```
src/templates/layout/master.hbs --> dist/index.html

<!-- AngualrJS - if CDN unavailable, load from local -->
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.22/angular.min.js"></script>
<script>typeof window.angular !== 'undefined' || document.write('<script src="scripts/libs/angular-1.2.22.min.js">\x3C/script>')</script>
```

now we need to tell Angular which area of the page it should operate within.  We are saying the whole page, but it does'nt have to be.  We do this by adding the ```ng-app``` attribute

```
src/templates/layout/master.hbs --> dist/index.html

<html class="no-js" ng-app="app" ng-controller="appCtrl">
```

I am also specifying an app Controller - this is where I will manage any $scope that is app wide.  Its not essential, but I like to have the higher level seperation of code.

The next task is to bind our app and initialise it in the JavaScript, including any module dependancies.  We will be using the Angular Routing module.

```
src/scripts/app.js --> dist/scripts/app.js

// App initialisation
var App = angular.module('app', ['ngRoute']);
```

#### Routing
Now a Route is the name usualy used to represent another page or view within a Single Page Application such as this.

This Demo, is just one view so does not require multiple route or Routing at all, but I still like to include it, for extendability, adding new views will be really easy.  e.g. a Chat history route.

```
src/scripts/router/router.js --> dist/scripts/app.js

// Router
App.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.
	when('/', {
		templateUrl: 'views/chat.html',
		controller: 'chatCtrl'
	}).
	otherwise({
		redirectTo: '/'
	});
}]);
```

Above we are specifing that on the default root route, we want to use the chat.html template, with the chatCtrl controller.  With a fallback for any other routes to redirect to ```/```

The output of the chat.html template will replace the ng-view directive in our page.

```
src/templates/pages/index.hbs --> dist/index.html

<body>
	<div ng-view></div>
</body>
```

###### Now, lets do the work to make our webapp special!

We are going to go through this systematically, to better show the flow of data, so there may be some jumping back and fourth between controllers, templates, services and the server code, but hang in there, I'm sure you will be able to see how each part connects to the next part.

##### So first, lets write some markup

**src/views/chat.html**
```
<section id="chat-area" class="chat-area">
	<div class="chat-history">
		<div ng-repeat="message in messages">
			<span class="name">{{message.name}}: </span>{{message.text}}
		</div>
	</div>
	<chat-input></chat-input>
</section>
```

This is the main area for the chat window.  We can see a containing section, a div to display the chat messages.  Here we are using the ```ng-repeat``` function to for each through the messages, outputting the message senders name and the message text itself.

And lastly we will being using our first directive.  This ```<chat-input>``` directive will contain all of the templates and code associated to display the chat input fields, the chat submit button, and the JavaScript functionality that we want to append to it.

#### The directive
As ```<chat-input>``` you may have noticed isn't a valid HTML tag, we need to tell Angular what to do with it. So we creative an angular directive for it.

**src/scripts/directives/chat-input.js**
```
// chat-input directive
App.directive('chatInput', function () {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/partials/chat-input.html',
		controller: 'chatInputCtrl'
	};
});
```

There is a name associated, so 'chat-input' becomes JS safe and we call it 'chatInput'.  We restrict it by 'E' for ~~elephant~~ element.  We say replace the whole tag, and then we specify a template for the directive and a controller.

**src/views/partials/chat-input.html**
```
<div id="chat-input" class="chat-input">
	<form>
		<input type="text" id="chat-text" ng-model="chatText" />
		<input type="submit" name="submit" value="Send" ng-click="sendMessage()"/>
	</form>
</div>
```

We could have just included this additional markup in the chat.html, and then defined the directive via restrict 'a' for ~~apple~~ attribute.  This is great for smaller directives, but as they grow, it is worth seperating the code.

At this point, we could run the app, and our in our browser we would see what looks like a very basic chat client.  But we want to add some functionality.  When somebody types into the input field and hits the submit button, we want to send it to our server.

You will see in ```chat-input.html``` there are two angular attributes;

* ```ng-model``` - this binds the value of the input box to the variable chatText within the current scope.  So that when we come to write our JavaScript in the Controller, we can get the value by calling $scope.chatText.

* ```ng-click``` - this is the action that will be taken upon... you guessed it... the click event of the Submit button.  ```sendMessage``` is a function that we have created onto the $scope in the controller.

#### The controller
```
// Chat Input Controller Controller
App.controller('chatInputCtrl', ['$scope','chat', function ($scope, chat) {
	// Set default value for input
	var resetChatBox = function () {
		$scope.chatText = '';
	};
	
	// On form submit function
	$scope.sendMessage = function () {
		chat.newMessage($scope.chatText);
		resetChatBox();
	};

	resetChatBox();
}]);
```

We can see in the first line of the controller, we are injecting the 'chat' service.  This is so that we can call methods that have been exposed, and remember it is the service that manages all the chat data, not the controller.

By setting ```$scope.chatText = ''``` this will automatically control the value of the input box, and shows the power of the two way data binding.  and then we see upon clicking of the submit button ```sendMessage```, we take the value of the input box ```$scope.chatText``` and we send it to the chat service ```chat.newMessage()``` that will handle the communication with the node server.

That brings us onto...

#### The services

The Chat service or factory, which is just a shortcut to the angular service.factory method, will handle all of our chat data, add, update, delete and communication with the server.

Take a look at ```src/scripts/services/chat.js```.  We can see it contains both public and private methods, and the variables in which we will keep the messages in memory for the app.
The chat service also injects the socket service.

```
App.factory('chat', ['$rootScope', 'socket', function ($rootScope, socket) {
	...
}]);
```

We do this, as we want all functionality around the websockets to be able to be shared with any other service that may require it.  We may in future create a Users service, where this data may be handled seperatly.

But lets get back to following the data flow, so from within our ```chatInputCtrl```, we called chat.newMessage().  At this point we created a new Message data object and sent it to the server using the ```socket.emit()``` method.

```
socket.emit('chat:message:new', message, callback);
```

The "chat:message:new" name is not important, its just a string, however this naming convention is really handy for keeping track of what messages are what when messages start flying back and fourth.

#### The Server
Our browser is now sending messages to the node server.  At this point I understand that many "Client side" developers may get a bit scared of doing JavaScript on a node server.  But do not fear, it is no different, if anything most node development is actually pretty poorly written, but simple JS.  I don't know if this is because "Software Engineers" have a go at JavaScript or what, but let me assure you it is not difficult at all.

Take a look at ```server-chat.js```

We are using express.io's route method to listen for any incoming websocket messages;

```
app.io.route('chat:message:new', function (req) {
	addMessage(req.data);
});
```

We then extract the data from the request, and call the addMessage function that adds that message to an Array called ```messages```.

Now there may be 1, 2, 100's of browsers attached to this node server, so we need to send the latest message out to all of them.  We do this by calling express.io's broadcast method

```
app.io.broadcast('chat:message:new', message);
```

There are otehr methods that may be used to send the message back to the browser that sent it, or all except the browser that sent it, but in this case we want to send the message back to ALL.

And that is it for the server for now, see, it wasn't that hard now was it?!? Let's get away from the server for a bit and get back to where us Web Dev's feel most comfortable, the client.

#### Back to the front-end

A quick recap; We built a front-end template, we took what the user entered into the input box, we sent it to the server, the server sent it back, and now we need to listen for that message to come back.

So if we go back to our ```chat.js``` service. We need to setup a listener.

```
socket.on('chat:message:new', function (data) {
	add(data);
});
```

We listen for any new messages and push it to the messages array in the chat service in the angular app in the browser.

##### THIS IS WHERE THE MAGIC HAPPENS!!

Do you remember our ```chatCtrl``` that had the chat services injected, and assigned ```chat.getMessages()``` to it's ```$scope```?? Well that data reference still exists!

So as soon as we pushed the new message to the messages array in our chat service, the $scope of our view was updated, the ```ng-repeat``` template updates and our new message appears in the ```.chat-history``` div as if by magic.  It's not magic, it never is, it's an illusion, but we havn't had to do all the underlying hard work to make that happen, Angular done gone and done that.

## Testing the concept
For this you might like to open a different browser, e.g. Chrome or Firefox, or open an Incognito window.  Have 2 or more windows open, and navigate to http://localhost:3000 in each. 

Type in a message, hit submit, and see it appears in all of your browser windows.

If you want to see something really cool.  Get the IP address of the computer with the nodeserver; ```ipconfig``` in your terminal window; then navigate to that IP address with the port 3000 on your telephone or tablet.  Provided you are on the same network/wifi - you can talk to one another.  And there it is, thats the Web Development "Money Shot". 

## Conclusion
Whilst you may look at teh rest of the project files @https://bitbucket.org/will-hancock/realtime-webapp-demo and find more code than I have described above, this is just simple tidying up to make it look nice.  I didn't want this to detract from the underlying methodology.

Feel free to have a look at how I approached the CSS styling, and other neat bit of JavaScript to round off the User Experience.

I hope that this post has been useful to both beginners and experts wanting to get to grips with Angular or Node.js, and if it has please do share on your social networks or to friends you think might be interested.

Until next time.



