﻿// chat-input directive
App.directive('chatInput', function () {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/partials/chat-input.html',
		controller: 'chatInputCtrl'
	};
});