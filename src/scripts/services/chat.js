﻿// Chat service

App.factory('chat', ['$rootScope', 'socket', function ($rootScope, socket) {

	var messages = [],

		Message = function (from, chatText) {
			this.text = chatText;
			this.name = from;
		},
		
		add = function (message) {
			messages.push(message);
		};

	/* LISTEN FOR INCOMING SOCKET MESSAGES */
	// new message
	socket.on('chat:message:new', function (data) {
		add(data);
	});

	// PUBLIC METHODS
	return {

		// ON - Listen for incoming socket messages
		getMessages: function () {
			return messages;
		},

		newMessage: function (chatText, callback) {
			var message = new Message('Will', chatText);
			socket.emit('chat:message:new', message, callback);
		}
		
	};

}
]);