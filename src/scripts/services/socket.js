﻿// Socket service

App.factory('socket', ['$rootScope', function ($rootScope) {

	var socket = io.connect(),

		// listen for message from server
		setupListener = function (eventName, callback) {
			console.log('listening for ' + eventName);
			socket.on(eventName, function () {
				var args = Array.prototype.slice.call(arguments);
				// add eventName to arguments
				args.push(eventName);

				console.log('received socket message: ', args);

				$rootScope.$apply(function () {
					callback.apply(socket, args);
				});
			});
		},

		// sendMessage to server
		sendMessage = function (eventName, data, callback) {

			console.log('sending socket message: ' + eventName, data);

			socket.emit(eventName, data, function () {
				var args = arguments;

				$rootScope.$apply(function () {
					if (callback) {
						callback.apply(socket, args);
					}
				});
			});
		},

		// send response on $rootScope to subscribed controllers
		broadcastMessage = function (data, eventName) {
			$rootScope.$broadcast(eventName, data);
		};

	// PUBLIC METHODS
	return {

		// ON - Listen for incoming socket messages
		on: setupListener,

		// EMIT - send out socket messages
		emit: sendMessage

	};

}
]);