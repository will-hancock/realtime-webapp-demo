﻿// Router
App.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.
	when('/', {
		templateUrl: 'views/chat.html',
		controller: 'chatCtrl'
	}).
	otherwise({
		redirectTo: '/'
	});
}]);