﻿// Index Controller
App.controller('chatCtrl', ['$scope','chat', function ($scope, chat) {
	$scope.messages = chat.getMessages();
}]);