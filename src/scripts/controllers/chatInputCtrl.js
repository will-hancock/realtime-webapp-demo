﻿// Chat Input Controller Controller
App.controller('chatInputCtrl', ['$scope','chat', function ($scope, chat) {

	// Set default value for input
	var resetChatBox = function () {
		$scope.chatText = '';
	};
	

	// On form submit function
	$scope.sendMessage = function () {
		chat.newMessage($scope.chatText);
		resetChatBox();
	};

	resetChatBox();

}]);